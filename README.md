# Mark Aarone Saturno

> I am a mobile developer based in the Philippines with 1 month of experience.

My focus area is in frontend development and I have been using Flutter for a month now.
Before that, I was using MERN stack which I learned through a coding boot camp. I am a people person that likes to collaborate with other persons to achieve a task/goal

# Contact Information

LinkedIn: l[inkedin.com/in/mark-aarone-saturno-1b4b771b9/](https://www.linkedin.com/in/mark-aarone-saturno-1b4b771b9/)

Website Portfolio:  [https://marksaturno.vercel.app/](https://marksaturno.vercel.app/)

# Work Experience

## Flutter Developer Trainee

FFUF, from July 21 to August 31

- I was trained to know dart, flutter, Dev Ops and some internal tools.
- I have worked with a pet project that involved converting a given design into actual UI.

# Skills

## Technical Skills

- Flutter
- Dart
- MERN

## Soft Skills

- Team Player
- Problem-Solver
- Communication Skills
- Teamwork

# Education

## BS Mechanical Engineer

Don Bosco Technical College, 2015-2020